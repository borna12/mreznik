import os
import io
from bs4 import BeautifulSoup

#otvaranj datoteka
f = io.open("leme.txt",'r+',encoding="utf-8")
f2 = io.open("deca.txt",'r+',encoding="utf-8")
f3 = io.open("stranci.txt",'r+',encoding="utf-8")
imenice = io.open("imenice.txt",'r+',encoding="utf-8")
brojevi = io.open("brojevi.txt",'r+',encoding="utf-8")
glagoli = io.open("glagol.txt",'r+',encoding="utf-8")
pridjevi = io.open("pridjevi.txt",'r+',encoding="utf-8")
zamjenice = io.open("zamjenice.txt",'r+',encoding="utf-8")

lines1 = f.read().replace("%i","").splitlines() # read old content
lines_deca = f2.read().replace("%i","").splitlines() # read old content
lines_stranci = f3.read().replace("%i","").splitlines() # read old content
lines_imenice = imenice.read().replace("%i","").splitlines() # read old content
lines_brojevi = brojevi.read().replace("%i","").splitlines() # read old content
lines_glagoli = glagoli.read().replace("%i","").splitlines() # read old content
lines_pridjevi = pridjevi.read().replace("%i","").splitlines() # read old content
lines_zamjenice = zamjenice.read().replace("%i","").splitlines() # read old content

[[x,lines1.count(x)] for x in set(lines1)]
rjecnik=dict((x,lines1.count(x)) for x in set(lines1))
rjecnik = dict(sorted(rjecnik.items(), key=lambda x: x[0].lower()))
abecedarij=io.open("Mrežnik_abecedarij.html","w",encoding="utf-8")

for k,v in rjecnik.items():
    klase="natuknica osnova"
    if k in lines_deca:
        klase+=" ucenici"
    if k in lines_stranci:
        klase+=" neizvorni"
    if k in lines_imenice:
        klase+=" imenice"
    if k in lines_brojevi:
        klase+=" brojevi"
    if k in lines_glagoli:
        klase+=" glagoli"
    if k in lines_pridjevi:
        klase+=" pridjevi"
    if k in lines_zamjenice:
        klase+=" zamjenice"
    if v ==1:
        abecedarij.write('<a href="natuknice/'+k.replace("ć","c").replace("š","s").replace("đ","d")+'.html" class="'+klase+'">'+k+'</a>\n')
    else:
        for x in range(1, v+1):
            abecedarij.write('<a href="natuknice/'+k.replace("ć","c").replace("š","s").replace("đ","d")+'_'+str(x)+'.html" class="'+klase+'">'+k+'<sup>'+str(x)+'</sup></a>\n')




abecedarij.close()

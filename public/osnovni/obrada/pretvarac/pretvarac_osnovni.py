import os
import io
from bs4 import BeautifulSoup
import re
import shutil


adresa="/".join(os.getcwd().split(os.sep)[:-1])+"/osnovni/"


abecedarij=""
for file in sorted(os.listdir(adresa)):
    if file.endswith(".html"):
        if  "%i" in str(file):
          os.rename(adresa+"/"+file,adresa+"/"+file.replace("%i",""))
          file=file.replace("%i","")
        f = io.open(adresa+"/"+file,'r+',encoding="utf-8")
        lines1 = f.readlines() # read old content
        soup = BeautifulSoup("\n".join(lines1), 'html.parser')
        for p in soup.find_all("p"): 
          p.attrs = {}
        tag = soup.find_all('span', {"class" : "vanjska_poveznica__poveznica"})
        for x in tag:
          try:
            x.name="a"
            x.attrs['href'] = x.string
            x.attrs['target'] = "_blank"
          except AttributeError:
            print(file)
        tvorbena=soup.find_all('span', {"class" : "Tvorbena_raz"})
        for x in tvorbena:
          try:
            reference=x.find_all('span')
            ref_broj="0"
            for y in reference:
              try:
                if y.attrs['class'][0]  == "Lemma__HomonymNumber":
                  ref_broj=y.string
                elif y.attrs['class'][0]  == "References":
                  y.name="a"
                  if ref_broj in ["1","2","3","4","5","6","7","8","9","10"]:
                    y.attrs['href'] = str(y.string.replace("ć","c").replace("š","s").replace("đ","d"))+"_"+ref_broj+".html"
                  else:
                    y.attrs['href'] = str(y.string.replace("ć","c").replace("š","s").replace("đ","d"))+".html"
              except AttributeError:
                print(file)
          except AttributeError:
            print(file)
        poveznice=soup.find_all('span', {"class" : "Poveznice"})
        for x in poveznice:
          try:
            reference=x.find_all('span')
            ref_broj="0"
            for y in reference:
              try:
                if y.attrs['class'][0]  == "Lemma__HomonymNumber":
                  ref_broj=y.string
                  print(ref_broj)
                elif y.attrs['class'][0]  == "References":
                  y.name="a"
                  if ref_broj in ["1","2","3","4","5","6","7","8","9","10"]:
                    y.attrs['href'] = str(y.string.replace("ć","c").replace("š","s").replace("đ","d"))+"_"+ref_broj+".html"
                  else:
                    y.attrs['href'] = str(y.string.replace("ć","c").replace("š","s").replace("đ","d"))+".html"
              except AttributeError:
                print(file)
          except AttributeError:
            print(file)
        span=soup.find('span', {"class" : "Lemma__LemmaSign"})
        span2=soup.find('span', {"class" : "Definicija__definicija"})
        try:
          natuknica=str(span.string.replace("%i",""))
          prvo_slovo=str(file[0])
          if str(file[-6]).isnumeric():
            abecedarij+='<a class="natuknica osnova" href="natuknice/'+str(file)+'">'+natuknica+ '<sup>'+str(file[-6])+'</sup></a>\n'
          else:
            abecedarij+='<a class="natuknica osnova" href="'+str(file)+'">'+natuknica+'</a>\n'
        except AttributeError:
          natuknica=""
          print("str replace")
        try:
          natuknica2=span2.string
        except AttributeError:
          natuknica2=""
        try:
          natuknica2=natuknica2.split("\n")
          natuknica2=natuknica2[0].lstrip().rstrip()
        except AttributeError:
          natuknica2=""
        razmak="\n"
        appendString = """<html><head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>"""+natuknica+""" – Mrežnik</title>
    <link rel="apple-touch-icon" sizes="57x57" href="../../apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../../apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../../apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../../apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../../apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../../apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../../apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../../apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../../apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../../android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../../favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../favicon-16x16.png">
    <link rel="manifest" href="../../manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="description" content='"""+natuknica2+"""'>
    <meta name="keywords" content="mrežni rječnik, hrvatski jezik">
    <meta name="author" content="Institut za hrvatski jezik i jezikoslovlje">
    <meta property="og:title" content='"""+natuknica+""" – Mrežnik' />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="../../og.jpg" />
    <meta property="og:description" 
  content='"""+natuknica2+"""' />
    <link href="../../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../css/stil.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui.js" type="text/javascript"></script><script src="../../js/index.js" type="text/javascript"></script>
    <style>
    </style>
</head>
<body text="#000000">
   <div id="zaglavlje">
   <a href="../../index.html"><img src="../../slike/logo.png" style="display:block;margin:auto; max-width:100%;height:auto;margin-top:20px" /></a>
    <p align="center" class="abeceda">
            <a  href="../../index.html#"> #</a> | <a  href="../../index.html#A">A</a> | <a  href="../../index.html#B">B</a> | <a 
            href="../../index.html#C">C</a> | <a  href="../../index.html#č">Č</a> | <a  href="../../index.html#ć">Ć</a> | <a  href="../../index.html#D">D</a> | <a  href="../../index.html#E">E</a> | <a  href="../../index.html#F">F</a> | <a 
            href="../../index.html#G">G</a> | <a  href="../../index.html#H">H</a> | <a  href="#I">I</a> | <a  href="../../index.html#J">J</a> | <a  href="../../index.html#K">K</a> | <a 
            href="../../index.html#L">L</a> | <a 
            href="../../index.html#Lj">Lj</a> | <a  href="../../index.html#M">M</a> | <a  href="../../index.html#N">N</a> | <a  href="../../index.html#Nj">Nj</a> |<a  href="../../index.html#O">O</a> | <a  href="../../index.html#P">P</a> | <a 
            href="../../index.html#R">R</a> | <a  href="../../index.html#S">S</a> | <a  href="../../index.html#š">Š</a> | <a  href="../../index.html#U">U</a> | <a  href="../../index.html#V">V</a> | <a 
            href="../../index.html#Z">Z</a> | <a  href="#ž">Ž</a></p>
    <form action="#" class="glossary__search__form">
        <input class="form-control" id="glossarySearchInput" placeholder="..." type="search" list="rijec" name="rijec">
        <datalist id="rijec" size="5">
          </datalist>
        <input type="button" value="pronađi" id="pronadi">
        <br>
    </form>
</div><script src="../../js/provjeri.js"></script>
<div class='kontejner'>
  <div id="birac">
  <form id="filteri">
  <label class="container" id="oo">osnovni
    <input type="checkbox" id="osnovni" onclick="provjeri1()">
    <span class="checkmark"></span>
  </label>
  <label class="container" id="ou">učenički
    <input type="checkbox" id="ucenicki" onclick="provjeri2()">
    <span class="checkmark"></span>
  </label>
  <label class="container" id="os">neizvorni govornici
    <input type="checkbox" id="stranci" onclick="provjeri3()">
    <span class="checkmark"></span>
  </label>
  </form>
</div>"""
        f.seek(0) # go back to the beginning of the file
        f.write(appendString) # write new content at the beginning
        # osnovni
        f.write("<div class='osnovni okvir'>"+str(soup)+"</div>")
        f.write('''</div><a href="#" class="scrollup">↑</a>
   <div id="donji_izbornik">
            <div class="kontejner">
                <img src="../../slike/zaklada_logo.png" class="slike_donje" style="float:left"><img src="../../slike/ihjj.png"
                    class="slike_donje" style="float:right">
            </div>

            <div class="kontejner" style="clear: both;	text-align: left;">
                <ul style="columns: 2;
    -webkit-columns: 2;
    -moz-columns: 2;">
                    <li><a href="">O rječniku</a></li>
                    <li><a href="http://ihjj.hr/mreznik/">Stranica projekta</a></li>
                    <li><a href="https://rjecnik.hr/igre">Igre</a></li>
                    <li><a href="https://borna12.gitlab.io/odostraznji/odostrazni-mreznik/">Odostražni rječnik</a></li>
                </ul>
            </div>

        </div>
    <footer>© <script>document.write(new Date().getFullYear())</script> Institut za hrvatski jezik i jezikoslovlje</footer>
    <script src="../../js/podnozje.js"></script></body></html>''')
        f.close()
source_dir = adresa
target_dir = '../../natuknice/ucenici'
    
    
file_names = os.listdir(source_dir)
  
for file_name in file_names:
  if file_name in os.listdir(target_dir):
    os.remove(target_dir+"/"+file_name)
    shutil.move(os.path.join(source_dir, file_name), target_dir)
  else:
    shutil.move(os.path.join(source_dir, file_name), target_dir)
              
        
datoteka=io.open("Mrežnik_abecedarij_osnovni.html","w",encoding="utf-8")
datoteka.write(abecedarij)
datoteka.close()
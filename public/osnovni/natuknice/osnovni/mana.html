<html><head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>mana – Mrežnik</title>
    <link rel="apple-touch-icon" sizes="57x57" href="../../apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="../../apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../../apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="../../apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../../apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="../../apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="../../apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="../../apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="../../apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="../../android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="../../favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../favicon-16x16.png">
    <link rel="manifest" href="../../manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="description" content='Mana je tjelesni ili funkcionalni nedostatak ili odstupanje od normalnih značajka.'>
    <meta name="keywords" content="mrežni rječnik, hrvatski jezik">
    <meta name="author" content="Institut za hrvatski jezik i jezikoslovlje">
    <meta property="og:title" content='mana – Mrežnik' />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="../../og.jpg" />
    <meta property="og:description" 
  content='Mana je tjelesni ili funkcionalni nedostatak ili odstupanje od normalnih značajka.' />
    <link href="../../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../css/stil.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui.js" type="text/javascript"></script><script src="../../js/index.js" type="text/javascript"></script>
    <style>
    </style>
</head>
<body text="#000000">
   <div id="zaglavlje">
   <a href="../../index.html"><img src="../../slike/logo.png" style="display:block;margin:auto; max-width:100%;height:auto;margin-top:20px" /></a>
    <p align="center" class="abeceda">
            <a  href="../../index.html#"> #</a> | <a  href="../../index.html#A">A</a> | <a  href="../../index.html#B">B</a> | <a 
            href="../../index.html#C">C</a> | <a  href="../../index.html#č">Č</a> | <a  href="../../index.html#ć">Ć</a> | <a  href="../../index.html#D">D</a> | <a  href="../../index.html#E">E</a> | <a  href="../../index.html#F">F</a> | <a 
            href="../../index.html#G">G</a> | <a  href="../../index.html#H">H</a> | <a  href="#I">I</a> | <a  href="../../index.html#J">J</a> | <a  href="../../index.html#K">K</a> | <a 
            href="../../index.html#L">L</a> | <a 
            href="../../index.html#Lj">Lj</a> | <a  href="../../index.html#M">M</a> | <a  href="../../index.html#N">N</a> | <a  href="../../index.html#Nj">Nj</a> |<a  href="../../index.html#O">O</a> | <a  href="../../index.html#P">P</a> | <a 
            href="../../index.html#R">R</a> | <a  href="../../index.html#S">S</a> | <a  href="../../index.html#š">Š</a> | <a  href="../../index.html#U">U</a> | <a  href="../../index.html#V">V</a> | <a 
            href="../../index.html#Z">Z</a> | <a  href="#ž">Ž</a></p>
    <form action="#" class="glossary__search__form">
        <input class="form-control" id="glossarySearchInput" placeholder="..." type="search" list="rijec" name="rijec">
        <datalist id="rijec" size="5">
          </datalist>
        <input type="button" value="pronađi" id="pronadi">
        <br>
    </form>
</div><script src="../../js/provjeri.js"></script>
<div class='kontejner'>
  <div id="birac">
  <form id="filteri">
  <label class="container" id="oo">osnovni
    <input type="checkbox" id="osnovni" onclick="provjeri1()">
    <span class="checkmark"></span>
  </label>
  <label class="container" id="ou">učenički
    <input type="checkbox" id="ucenicki" onclick="provjeri2()">
    <span class="checkmark"></span>
  </label>
  <label class="container" id="os">neizvorni govornici
    <input type="checkbox" id="stranci" onclick="provjeri3()">
    <span class="checkmark"></span>
  </label>
  </form>
</div><div class='osnovni okvir'>
<p><span class="Lemma"> <span class="Lemma__Naglaseno">mána </span><span class="Lemma__LemmaSign">mana</span> <span class="Lemma__vrsta_rijeci">im. ž.</span><span class="Gramaticki_blok"><span class="Oblici"> (<span class="Oblici__naglaseni">G mánē, D máni, A mánu, L máni, I mánōm; <i>mn.</i> NA mána, G mánā, DLI mánama</span>)</span></span></span></p>
<p><span class="Sense"> <span class="Sense__SenseNumber">1</span> <span class="Definicija"><span class="Definicija__definicija"> Mana je tjelesni ili funkcionalni nedostatak ili odstupanje od normalnih značajka.</span></span><span class="primjeri"> <span class="Primjer"></span></span></span></p>
<p><span class="Primjer__Primjer"> - Oko 40 % djece s Downovim sindromom ima prirođenu srčanu manu.</span></p>
<span class="Primjer"></span>
<p><span class="Primjer__Primjer"> - Zastao na vrhu stubišta i bez zamuckivanja počeo brzati kao da je želio da što više kaže i ugrabi prije no mu se govorna mana povrati.</span></p>
<span class="Primjer"></span>
<p><span class="Primjer__Primjer"> - Birači koji ne mogu glasovati zbog tjelesne mane ili su nepismeni, mogu na glasovanje doći s pismenom osobom.</span></p>
<p><span class="kolokacije"></span></p>
<p><span class="Kolokacija"> <span class="Kolokacija__odrednica">Kakva je mana?</span> <span class="Kolokacija__kolokacija">česta, genska, govorna, prirođena, složena, srčana, tjelesna, urođena</span></span></p>
<p><span class="Kolokacija"> <span class="Kolokacija__odrednica">Koordinacija:</span> <span class="Kolokacija__kolokacija">mana i bolest</span></span></p>
<p><span class="Sense"> <span class="Sense__SenseNumber">2</span> <span class="Definicija"><span class="Definicija__definicija"> Mana je negativna osobina ili svojstvo.</span></span><span class="primjeri"> <span class="Primjer"></span></span></span></p>
<p><span class="Primjer__Primjer"> - Gord si, Matija, i ohol, to ti je jedina mana.</span></p>
<span class="Primjer"></span>
<p><span class="Primjer__Primjer"> - Jedina mana takvih mirisa je što brzo ishlape, pa vam se već nakon nekoliko sati može učiniti kao da se uopće niste namirisali.</span></p>
<span class="Primjer"></span>
<p><span class="Primjer__Primjer"> - Nakon dugih godina oporavka i uza sve poznate mane i propuste, turizam je postao profitabilan hrvatski proizvod.</span></p>
<span class="Primjer"></span>
<p><span class="Primjer__Primjer"> -  Mana koju smo uočili je vidljivost piksela, a kada ih jednom uočite teško ih je ne vidjeti u daljnjem korištenju. </span></p>
<span class="Primjer"></span>
<p><span class="Primjer__Primjer"> - Čitatelju se otvara u svim svojim manama i nesavršenostima koje je čine posebnom. </span></p>
<p><span class="kolokacije"></span></p>
<p><span class="Kolokacija"> <span class="Kolokacija__odrednica">Kakva je mana?</span> <span class="Kolokacija__kolokacija">česta, jedina, karakterna, očita, sitna; partnerova, ženska</span></span></p>
<p><span class="Kolokacija"> <span class="Kolokacija__odrednica">Što se s manom može?</span> <span class="Kolokacija__kolokacija">ismijavati je, ispraviti je, naći je, pronaći je, skriti je, tolerirati je, uvidjeti je; nabrajati ih</span></span></p>
<p><span class="Kolokacija"> <span class="Kolokacija__odrednica">Koordinacija:</span> <span class="Kolokacija__kolokacija">mana i nesavršenost, mana i pogreška, mana i prednost, mana i propust, mana i slabost, mana i vrlina,</span></span></p>
<p><span class="Kolokacija"> <span class="Kolokacija__odrednica">U vezi s manom spominje se: </span></span></p>
<p><span class="Poveznice"> <a class="References">SINONIM:  <a class="References" href="nedostatak.html">nedostatak</a> :3</a></span></p>
<p><span class="vanjska_poveznica"> <span class="vanjska_poveznica__izvor">Struna: </span> <a class="vanjska_poveznica__poveznica" href="http://struna.ihjj.hr/naziv/mana/23736/#naziv" target="_blank">http://struna.ihjj.hr/naziv/mana/23736/#naziv</a></span></p>
<p><span class="vanjska_poveznica"> <span class="vanjska_poveznica__izvor">Kolokacijska baza hrvatskoga jezika: </span> <a class="vanjska_poveznica__poveznica" href="http://ihjj.hr/kolokacije/search/?q=mana&amp;search_type=basic" target="_blank">http://ihjj.hr/kolokacije/search/?q=mana&amp;search_type=basic</a></span></p>
</div></div><a href="#" class="scrollup">↑</a>
   <div id="donji_izbornik">
            <div class="kontejner">
                <img src="../../slike/zaklada_logo.png" class="slike_donje" style="float:left"><img src="../../slike/ihjj.png"
                    class="slike_donje" style="float:right">
            </div>

            <div class="kontejner" style="clear: both;	text-align: left;">
                <ul style="columns: 2;
    -webkit-columns: 2;
    -moz-columns: 2;">
                    <li><a href="">O rječniku</a></li>
                    <li><a href="http://ihjj.hr/mreznik/">Stranica projekta</a></li>
                    <li><a href="https://rjecnik.hr/igre">Igre</a></li>
                    <li><a href="https://borna12.gitlab.io/odostraznji/odostrazni-mreznik/">Odostražni rječnik</a></li>
                </ul>
            </div>

        </div>
    <footer>© <script>document.write(new Date().getFullYear())</script> Institut za hrvatski jezik i jezikoslovlje</footer>
    <script src="../../js/podnozje.js"></script></body></html>
function provjeri1() {
    $(".okvir").css({
        "border-bottom": "1px solid rgba(0, 157, 224, 0.5)"
    })
    if (document.getElementById('osnovni').checked) {
        $(".osnovni").show();
        $('html, body').animate({
            scrollTop: ($('.osnovni').offset().top)
        }, 500);
    } else {
        $(".osnovni").hide();
    }
    $('.okvir').not(':hidden').last().css({
        "border-bottom": "0"
    })

}

function provjeri2() {
    $(".okvir").css({
        "border-bottom": "1px solid rgba(0, 157, 224, 0.5)"
    })

    if (document.getElementById('ucenicki').checked) {
        $(".deca").show();
        $('html, body').animate({
            scrollTop: ($('.deca').offset().top)
        }, 500);
    } else {
        $(".deca").hide();
    }
    $('.okvir').not(':hidden').last().css({
        "border-bottom": "0"
    })

}

function provjeri3() {
    $(".okvir").css({
        "border-bottom": "1px solid rgba(0, 157, 224, 0.5)"
    })

    if (document.getElementById('stranci').checked) {
        $(".stranci").show();
        $('html, body').animate({
            scrollTop: ($('.stranci').first().offset().top)
        }, 500);
    } else {
        $(".stranci").hide();
    }
    $('.okvir').not(':hidden').last().css({
        "border-bottom": "0"
    })

}
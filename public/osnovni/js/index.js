function stvori(x) {
    y = x + ".html";
    y = y.replace(/č/g, "c");
    y = y.replace(/ć/g, "c");
    y = y.replace(/š/g, "s");
    y = y.replace(/ž/g, "z");
    y = y.replace(/đ/g, "d");
    window.location.href = y;
}

function remove_hash_from_url() {
    var uri = window.location.toString();
    if (uri.indexOf("#") > 0) {
        var clean_uri = uri.substring(0,
            uri.indexOf("#"));
        window.history.replaceState({},
            document.title, clean_uri);
    }
}

$(function() {
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
                panel.style.padding = "0 18px";
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
                panel.style.padding = "18px 18px";

            }
        });
    }
    initGlossaryFilter();

    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 400);
        return false;
    });
 if (window.location.href.includes("#")) {
        upit = decodeURI(window.location.href.split("#")[1])
        if (upit.length >= 3) {
            $('#glossarySearchInput').val(upit)
            $("#pronadi").click()
        }
    }
});

function initGlossaryFilter() {
    // Filter using search box
    $("#glossarySearchInput").keypress(function(e) {
  //Enter key
  if (e.which == 13) {
      $("#pronadi").click()
    return false;
      
  }
});


    $("#pronadi").on("click", function(e) {
        $('select').val("osnova");
        remove_hash_from_url
        var inputValue = $('#glossarySearchInput').val();
        parent.location.hash = inputValue;
        // Hide all the results & Cards
        $(".gnja").addClass("inactive");
        $(".natuknica").hide();
        $(".gnja").each(function() {
            $(".natuknica").each(function() {
                var item = $(this).html();
                if (item.toUpperCase().indexOf(inputValue.toUpperCase()) != -1) {
                    $(this).parents(".gnja").removeClass("inactive");
                    $(this).show();
                }
            });
        });
        if ($('.natuknica:hidden').length == $('.natuknica').length) {
            $("#rez").fadeIn(600).delay(600).fadeOut(600);
        }
    });

}

function prikazi_sve() {
    $(".gnja").removeClass("inactive");
    $(".natuknica").show();
}